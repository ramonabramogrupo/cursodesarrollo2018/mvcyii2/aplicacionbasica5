<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Trabajadores;
use app\models\Delegacion;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta12(){
                
        return $this->render("consulta12",[
            "modelos"=>Trabajadores::find()->all(),
        ]);
    }
    
    public function actionConsulta14(){
        /**
         * activeRecord
        */
        $a= Delegacion::find()->all();
        
        /**
         * command
         */
        
        $b=Yii::$app
                ->db
                ->createCommand("select * from delegacion")
                ->queryAll();
        
        /**
         * query builder
         */
        
        $query=new \yii\db\Query();
        $c=$query
                ->select('*')
                ->from('delegacion')
                ->all();
        
        
        
        return $this->render("consulta14",[
            "uno"=>$a,
            "dos"=>$b,
            "tres"=>$c,
        ]);
    }
    
    public function actionConsulta17(){
        $a=[];
        
        /**
         * primera consulta
         */
        
        $a[]= Delegacion::find()
                ->where('poblacion="santander"')
                ->all();
        
        $a[]= Delegacion::find()
                ->where(['poblacion'=>"santander"])
                ->all();
        
        $a[]=Yii::$app
                ->db
                ->createCommand("SELECT * FROM delegacion where poblacion='santander'")
                ->queryAll();
        
        /*
         * segunda consulta
         */
        
        $a[]= Trabajadores::find()
                ->where(["delegacion"=>1])
                ->all();
        
        $a[]=Yii::$add
                ->createCommand("select * from trabajadores where delegacion=1")
                ->queryAll();
                
        /* 
         * tercera consulta 
         * 
         */
                
        $a[]= Trabajadores::find()
                ->orderBy("nombre")
                ->all();
        
        $a[]= Trabajadores::find()
                ->orderBy([
                    "nombre"=>SORT_ASC
                ])
                ->all();
        
        $a[]=Yii::$app
                ->createCommand("Select * from trabajadores order by nombre ASC")
                ->queryAll();
        
                
        
        return $this->render("consulta17",[
            "resultados"=>$a,
        ]);
    }
    
    public function actionConsulta20(){
        

        $datos=[];
        /* consulta a */
        $datos[]=Delegacion::find()
                ->where(["<>", "poblacion","santander"])
                ->andWhere(["IS NOT","direccion",null])
                ->all();
        
        $datos[]=Delegacion::find()
                ->where('poblacion not like "santander" and direccion is not null')
                ->all();
        
        /* consulta b */
        $datos[]= Trabajadores::find()
                ->joinWith("delegacion0")
                ->where(["poblacion"=>"santander"])
                ->asArray()
                ->all();
        
        $datos[]=Trabajadores::find()
                ->joinWith("delegacionSantander",TRUE,"INNER JOIN")
                ->all();
             
        /* optimizarla yo */
        
        $a=Delegacion::find()
                ->select("id")
                ->where(["poblacion"=>"santander"])
                ->asArray()
                ->all();
        $a=\yii\helpers\ArrayHelper::map($a,"id","id");

        $datos[]=Trabajadores::find()
                ->where(["in","delegacion",$a])
                ->all();
        
        $datos[]=Trabajadores::find()
                ->where(["IN","delegacion", Delegacion::getDelegacionesId()])
                ->asArray()
                ->all();
        
        /* consulta c */
        $datos[]=Trabajadores::find()
                ->with('delegacion0')
                ->where('foto is not null')
                ->all();
        
                
        /* consulta d */
        $datos[]= Delegacion::find()
                ->joinWith("trabajadores t")
                ->where(["t.delegacion"=>null])
                ->asArray()
                ->all();
        
        return $this->render("consulta20",[
                "datos"=>$datos
                ]);
    }
    
    public function actionConsulta22(){
        $datos =[];
        $datos[]=Trabajadores::getConsulta1();
        $datos[]=Trabajadores::getConsulta2();
        $datos[]=Trabajadores::getConsulta3();
        $datos[]=Trabajadores::getConsulta4();
       return $this->render("consulta22",[
           "datos"=>$datos
       ]) ;
    }
    
    
            
}
