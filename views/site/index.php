<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Consultas';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas basicas con Yii</h1>
        <p><a class="btn btn-primary btn-lg" role="button" href="<?= Url::to(["consulta12"]); ?>">Consulta12</a></p>
        <p><a class="btn btn-primary btn-lg" href="<?= Url::to(["consulta14"]); ?>">Consulta14</a></p>
        <p><a class="btn btn-primary btn-lg" href="<?= Url::to(["consulta20"]); ?>">Consulta20</a></p>
        <p><?= yii\helpers\Html::a("Consulta 22", ["consulta22"], ["class" => "btn btn-primary btn-lg"]) ?></p>
    </div>
</div>

